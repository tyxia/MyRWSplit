package com.freedom.mysql.myrwsplit.runnable;

public abstract class BaseRunnable<T> {
	public abstract T run() throws Exception;
}
